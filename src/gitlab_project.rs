use serde::{Deserialize, Serialize};

// Not all implemented only once we use at this moment.
#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct GitLabProject {
    pub id: u32,
    pub description: Option<String>,
    pub name: String,
    pub path: String,
    pub path_with_namespace: String,
    pub archived: bool,

    pub default_branch: String,

    pub ssh_url_to_repo: String,
}
