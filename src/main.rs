mod gitlab_project;

use anyhow::Error;
use clap::Parser;
use console::Term;
pub use gitlab_project::GitLabProject;
use reqwest::{header::HeaderValue, StatusCode};
use serde::de::DeserializeOwned;
use simple_logger::SimpleLogger;
use std::{
    io::Write,
    path::{Path, PathBuf},
    process::Command,
    thread::sleep,
    time::Duration,
};

type GitLabGroupId = String;

#[derive(Debug, Default, Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// The ID of the GitLab group.
    /// This is usually a number.
    pub gitlab_group_id: GitLabGroupId,

    // /// Path to the config file to be used.
    // /// If no path is set it will use `./download-gitlab-group-config.json` if it exists.
    // #[arg(short, long)]
    // pub config_path: Option<PathBuf>,
    /// The main URL where the gitlab instance can be found.
    /// Default: `gitlab.com`
    #[arg(long)]
    pub gitlab_url: Option<String>,

    /// The folder the GitLab instance will be cloned to.
    /// Default is the current working directory.
    #[arg(long, value_name = "FOLDER")]
    pub download_folder: Option<PathBuf>,

    /// If set to `true` it will try to check out the default branch again.
    /// This will not succeed when local changes are made that are not committed.
    #[arg(long)]
    pub checkout_default_branch: bool,

    /// List of group names to not download.
    /// Group name must be an exact match.
    #[arg(long)]
    pub exclude_group_names: Vec<String>,

    /// JSON File where the list of projects will be found.
    /// If this option is present it will not use the GitLab API to download the list of projects.
    /// This expected format is the same json format as it would have received from the GitLab API.
    #[arg(long)]
    pub project_list_from_file: Option<PathBuf>,
}

fn main() {
    SimpleLogger::new()
        .without_timestamps()
        .with_level(log::LevelFilter::Debug)
        .init()
        .unwrap();
    let cli = Cli::parse();
    let term = Term::stdout();

    // Get project list from file or GitLab API.
    let project_list: Vec<GitLabProject> = if let Some(project_list_file) =
        cli.project_list_from_file
    {
        parse_json_file(&project_list_file).expect("Could not parse project file")
    } else {
        let gitlab_url = cli.gitlab_url.unwrap_or("gitlab.com".to_owned());
        println!(
            "To download the list of projects we need a GitLab API Key.\n\
            You can create a personal access token here: \n\
            https://{gitlab_url}/-/profile/personal_access_tokens?name=Download+GitLab+Projects&scopes=read_api"
        );
        print!("Please enter your API Key:");
        std::io::stdout().flush().unwrap();
        let gitlab_api_key = term
            .read_secure_line()
            .expect("Could not read key from terminal")
            .trim()
            .to_owned();

        download_project_list(cli.gitlab_group_id, gitlab_url, gitlab_api_key)
    };

    log::info!("Found in GitLab: {}", project_list.len());
    // Filter
    let project_list = filter_projects(project_list, cli.exclude_group_names);
    log::info!("After filtering: {}", project_list.len());

    println!(
        "Projects: {:#?}",
        project_list
            .iter()
            .map(|p| p.path_with_namespace.clone())
            .collect::<Vec<_>>()
    );

    println!("Going to downloading the {} repo's listed above. Press any key to continue. Press `ctrl+c` (`^c`) to quit.", project_list.len());
    term.read_key().expect("Could not read key from terminal.");

    // Download repos
    download_or_update_repos(
        project_list,
        cli.download_folder.unwrap_or(PathBuf::from("./")),
        cli.checkout_default_branch,
    );
}

/// Download and merge all projects using the GitLab API.
fn download_project_list(
    group_id: GitLabGroupId,
    gitlab_url: String,
    gitlab_api_key: String,
) -> Vec<GitLabProject> {
    // Create download URL
    let template_url = format!("https://{gitlab_url}/api/v4/groups/{group_id}/projects?include_subgroups=true&per_page=100");

    let mut page_counter = 1;
    let mut empty_page_found = false;
    let mut project_list: Vec<GitLabProject> = vec![];
    let client = reqwest::blocking::Client::new();
    // Limit amount of pages and prevent infinite loops.
    while page_counter < 1000 && !empty_page_found {
        log::info!("Downloading page {page_counter} from GitLab API.");
        // Download current page
        let page_url = format!("{template_url}&page={page_counter}");

        let response = client
            .get(page_url)
            .header(
                "Authorization",
                HeaderValue::from_str(&format!("Bearer {}", &gitlab_api_key)).unwrap(),
            )
            .send()
            .expect("Could not get list of projects from GitLab API.");

        let mut requested_projects = match response.status() {
            StatusCode::OK => response
                .json::<Vec<GitLabProject>>()
                .expect("GitLab API response was not a list of projects."),
            _ => {
                log::error!(
                    "Unexpected response:\n{}",
                    response.text().unwrap_or_default()
                );
                panic!("Error when requesting list of projects.");
            }
        };

        empty_page_found = requested_projects.is_empty();

        project_list.append(&mut requested_projects);

        page_counter += 1;
    }
    project_list
}

/// Filter out groups or projects you don't want to download.
/// This also filters out all archived projects.
fn filter_projects(
    projects: Vec<GitLabProject>,
    exclude_group_names: Vec<String>,
) -> Vec<GitLabProject> {
    projects
        .into_iter()
        .filter(|project| {
            // Skip all archive projects
            if project.archived {
                return false;
            }

            // Check if project contains group that should be ignored.
            if project
                .path_with_namespace
                .split('/')
                .any(|name| exclude_group_names.contains(&name.to_owned()))
            {
                return false;
            }

            // keep all others.
            true
        })
        .collect()
}

/// Clone the repo to the local computer if it does not exists.
/// Update (pull) the repo if it was already cloned.
fn download_or_update_repos(
    projects: Vec<GitLabProject>,
    download_folder: PathBuf,
    checkout_default_branch: bool,
) {
    let project_number = projects.len();
    let mut delay;
    for (i, project) in projects.iter().enumerate() {
        log::info!("Project {}/{} {}", i, project_number, project.name);

        // Skip some of them
        // if i < 99 {
        //     println!("Skipped");
        //     continue;
        // }
        // prevent downloading everything
        // if i > 300 {
        //     break;
        // }

        let current_project_folder = project.path_with_namespace.replace('/', "__");
        let project_local_dir = download_folder.join(PathBuf::from(&current_project_folder));

        if Path::new(&project_local_dir).exists() {
            log::info!("Fetching updates");
            // if folder already exists -> fetch
            Command::new("git")
                .current_dir(&project_local_dir)
                .args(["fetch"])
                .spawn()
                .expect("failed to execute git fetch")
                .wait()
                .expect("Fetch did not run");
            delay = Duration::from_micros(20);
        } else {
            log::info!("Cloning repo");
            // if folder does not already exists -> clone
            Command::new("git")
                .current_dir(&download_folder)
                .args(["clone", &project.ssh_url_to_repo, &current_project_folder])
                .spawn()
                .expect("failed to execute git clone")
                .wait()
                .expect("Clone did not run");
            delay = Duration::from_micros(100);
        }

        if checkout_default_branch {
            // Checkout default branch
            Command::new("git")
                .current_dir(&project_local_dir)
                .args(["checkout", &project.default_branch])
                .spawn()
                .expect("failed to checkout default branch")
                .wait()
                .expect("Checkout did not run");
        }

        log::info!("Waiting");
        sleep(delay);
    }
}

fn parse_json_file<C: DeserializeOwned>(file_path: &PathBuf) -> Result<C, Error> {
    let content_file =
        std::fs::read_to_string(file_path).expect("Should have been able to read the file");
    let parsed_result = &mut serde_json::de::Deserializer::from_str(&content_file);
    let result: Result<C, _> = serde_path_to_error::deserialize(parsed_result);
    let parsed_object: C = match result {
        Ok(data) => data,
        Err(err) => {
            let path = err.path().to_string();
            log::error!("Error: {} \nIn: {}", err, path);
            return Err(Error::from(err));
        }
    };
    Ok(parsed_object)
}
